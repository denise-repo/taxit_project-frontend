import { Injectable } from '@angular/core';
import { CanActivate, Router } from "@angular/router";
import { AuthenticateService } from "../core/services/authenticate.service";

@Injectable({ providedIn: 'root' })
export class HomeGuardService implements CanActivate {


  constructor(
    private router: Router,
    private authenticate: AuthenticateService
  )
  { }

  canActivate(): boolean {
    if (!this.authenticate.isAutenticated()) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
