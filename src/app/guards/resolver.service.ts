import {Resolve} from "@angular/router";
import {Injectable} from "@angular/core";
import {UsersService} from "../core/services/users.service";
@Injectable({ providedIn: 'root' })



export class ResolverService implements Resolve<any> {
  constructor(
    private usersService: UsersService
  ) {
  }
  resolve() {
    return this.usersService.getUser(localStorage.getItem('userId'));
  }
}
