import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['../assets/scss/app.scss']
})
export class AppComponent implements OnInit{
  title = 'Denise Project';

  constructor( private _router: Router ) { }
  ngOnInit() {}

}
