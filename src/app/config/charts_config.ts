
//Gender pie Chart
export const genderPieChartOptions = {
  titlePosition: 'none',
  legend: { position: 'bottom' },
  pieHole: 0.4,
  colors: ['#f0ad4e', '#d9534f', '#5bc0de', '#f6c7b6'],
  sliceVisibilityThreshold: 0,
  width:400,
  height:300
};


//Totals column chart
export const totalChartOptions = {
  titlePosition: 'none',
  vAxis: { title: 'Total', minValue: 0, format: '#,###' },
  hAxis: { title: 'Quantidade' },
  legend: { position: 'none' },
  colors:  ['#5bc0de', '#f6c7b6']
};

//Entries by month column chart
export const entriesChartOptions = {
  titlePosition: 'none',
  vAxis: { title: 'Meses', minValue: 0, format: '#,###' },
  hAxis: { title: 'Total de entradas' },
  legend: { position: 'none' },
  colors:  ['#f6c7b6']
};

//Variable that exports all variables with options data for Google Charts examples

export const dashboardData = {
  genderPieChartOptions,
  totalChartOptions,
  entriesChartOptions
};
