export const columns = {
  'user': [
    {columnDef: 'first_name', header: 'Primeiro Nome', cell: (element: any) => `${element.first_name}`},
    {columnDef: 'last_name', header: 'Último Nome', cell: (element: any) => `${element.last_name}`},
    {columnDef: 'phone', header: 'Telefone', cell: (element: any) => `${element.phone}`},
    {columnDef: 'rank', header: 'Profissão', cell: (element: any) => `${element.rank}`},
    {columnDef: 'entry_date', header: 'Data de entrada', cell: (element: any) => `${element.entry_date}`},
  ],
  'projects': [
    {columnDef: 'name', header: 'Nome do Projecto', tooltip: 'Company Name', cell: (element: any) => `${element.name}`},
    {columnDef: 'start_date', header: 'Date de Inicío', tooltip: 'Data de Início', cell: (element: any) => `${element.start_date}`},
    {columnDef: 'end_date', header: 'Data de Fim', tooltip: 'Data de Fim', cell: (element: any) => `${element.end_date}`},
    {columnDef: 'status', header: 'Status', tooltip: 'Status', cell: (element: any) => `${element.end_date}`},
  ],
}




