import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { ProjectComponent } from './pages/project/project.component';
import { TechnologyComponent } from './pages/technology/technology.component';
import { UsersComponent } from './pages/users/users.component';
import { CreateUserComponent } from './pages/users/create-user/create-user.component';
import { CreateProjectComponent } from './pages/project/create-project/create-project.component';
import { EditUserComponent } from './pages/users/edit-user/edit-user.component';
import { UserInfoComponent } from './pages/user-info/user-info.component';
import { EditProjectComponent } from './pages/project/edit-project/edit-project.component';
import { MatConfirmDialogComponent } from './mat-confirm-dialog/mat-confirm-dialog.component';
import { CreateTechnologyComponent } from "./pages/technology/create-technology/create-technology.component";
import { EditTechologyComponent } from './pages/technology/edit-techology/edit-techology.component';

//services
import { UsersService } from "./core/services/users.service";
import { ProjectsService } from "./core/services/projects.service";

//angular module
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { AngularMaterialModule } from "./core/angular-material/angular-material.module";
import { MatFormFieldModule } from "@angular/material/form-field";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DatePipe } from "@angular/common";
import { ToastrModule } from "ngx-toastr";
import {MatDialogModule } from "@angular/material/dialog";
import {GoogleChartsModule} from "angular-google-charts";
import {PhonePipe} from "./pipes/phone.pipe";
import { LoginComponent } from './pages/login/login.component';
import {AuthenticateInterceptor} from "./interceptors/authenticate.interceptor";



@NgModule({
  declarations: [
    AppComponent,
    ProjectComponent,
    UsersComponent,
    HomepageComponent,
    TechnologyComponent,
    CreateUserComponent,
    CreateProjectComponent,
    UserInfoComponent,
    EditUserComponent,
    EditProjectComponent,
    MatConfirmDialogComponent,
    CreateTechnologyComponent,
    EditTechologyComponent,
    PhonePipe,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatTableModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    GoogleChartsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    UsersService,
    ProjectsService,
    DatePipe,
    { provide: HTTP_INTERCEPTORS,
      useClass: AuthenticateInterceptor,
      multi: true }
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
