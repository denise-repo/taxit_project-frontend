export interface Technology {
  id: number;
  name: string,
  created_at: string,
}
