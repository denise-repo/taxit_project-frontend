export interface User {
  id: number
  first_name: string;
  last_name: string;
  phone: number;
  rank: string;
  entry_date: string;
  email: string;
  age: string;
  gender: string;
  username: string;
  password: string;
  token: string;
  permissions: string[];

}
