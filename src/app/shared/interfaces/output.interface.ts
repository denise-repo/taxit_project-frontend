export interface Output {
  id: number
  file_name: string
  file_type: string
}
