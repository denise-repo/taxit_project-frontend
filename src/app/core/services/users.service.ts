import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  userApi = "http://localhost:3000/api/users";

  constructor(
    private _http: HttpClient
  ) {
  }

  // Endpoint to get the list of all users
  getUsersList() {
    return this._http.get(this.userApi);
  }

  //Endpoint to get specific user
  //param userId
  getUser(userId: string | null) {
    return this._http.get(`${this.userApi}/${userId}`);
  }

  //Endpoint to create new user
  //param userId
  createUser(user: any) {
    const headers = {'content-type': 'application/json'}
    return this._http.post(this.userApi, user, {headers: headers}).toPromise().then(
      response => {
        return response;
      });
  }

  //description Endpoint to update users
  //param user
  updateUser(user: any): Observable<any> {
    return this._http.put(`${this.userApi}/${user.id}`, user)
  }

  //description Endpoint to delete current user
  //param userId
  deleteUser(userId: number) {
    return this._http.delete(this.userApi + "/" + userId);
  }

  //description Endpoint to get all projects from a specific user
  //param userId
  getAllProjectsFromUser(userId: any) {
    return this._http.get(`${this.userApi}/${userId}/user_projects`)

  }

  //'users/import_file' => 'users#import'
  importFile(file: any) {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    return this._http.post(`${this.userApi}/import_file`, formData)
  }

  //'users/:id/import_pdf'
  importUsersPdf(userId: any, file: any) {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    return this._http.post(`${this.userApi}/${userId}/import_pdf`, formData)
  }

  //api/users/:user_id/download_pdf/:attachment_id
  downloadPdfAttachment(userId: any) {
    return this._http.get(`${this.userApi}/${userId}/download_pdf`)
  }

}
