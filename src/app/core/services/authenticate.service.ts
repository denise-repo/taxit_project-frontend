import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {User} from "../../shared/interfaces/user.interface";


@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {
  baseUrl: string = 'http://localhost:3000/api';

  constructor(
    private http: HttpClient
  ) { }


  authenticate(username: string, password: string) {
    return this.http.post<User>(`${this.baseUrl}/login`, { username, password });
  }

  isAutenticated(): boolean {
    return localStorage.getItem('token') !== null;
  }
}
