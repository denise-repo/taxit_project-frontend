import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TechnologyService {

  technologyApi = "http://localhost:3000/api/technologies";

  constructor(
    private _http: HttpClient
  ) { }

  // Endpoint to get the list of all technologies
  getTechnologiesList() {
    return this._http.get(this.technologyApi);
  }

  // Endpoint to get a specific technology
  getTechnology(technologyId: number) {
    return this._http.get(`${this.technologyApi}/${technologyId}`)
  }

  // Endpoint to create new technology
  // @param technology
  createTechnology(technology: any) {
    const headers = {'content-type': 'application/json'}
    return this._http.post(this.technologyApi, technology, {headers: headers}).toPromise().then(
      response => {
        return response;
      });
  }

  //Endpoint to update technology
  // @param technology
  updateTechnology(technology: any): Observable<any> {
    return this._http.put(`${this.technologyApi}/${technology.id}`, technology);
  }

  //description Endpoint to delete technology
  //param id
  deleteTechnology(id: number) {
    return this._http.delete(this.technologyApi + "/" + id);
  }
}
