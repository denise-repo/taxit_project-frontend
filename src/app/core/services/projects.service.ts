import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../../shared/interfaces/user.interface";
import {ActivatedRoute} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  projectsApi = "http://localhost:3000/api/projects";

  constructor(
    private _http: HttpClient,
    private _activatedRoute: ActivatedRoute
  ) {
  }

  //Endpoint to get the list of all projects
  getProjectList() {
    return this._http.get(this.projectsApi);
  }

  //Endpoint to get specific project
  getProject(projectId: number) {
    return this._http.get(`${this.projectsApi}/${projectId}`)
  }

  //Endpoint to create new project
  //param Project
  createProject(project: any) {
    const headers = {'content-type': 'application/json'}
    return this._http.post(this.projectsApi, project, {headers: headers}).toPromise().then(
      response => {
        return response;
      });
  }

  //Endpoint to update project
  //param Project
  updateProject(project: any): Observable<any> {
    return this._http.put(`${this.projectsApi}/${project.id}`, project);
  }

  deleteProject(projectId: number) {
    return this._http.delete(`${this.projectsApi}/${projectId}`)

  }

  //POST api/project/:id/user/:user_id/update_user_project
  userProjects(projectId: any, userId: any): Observable<any> {
    return this._http.post(this.projectsApi + "/" + projectId + "/user/" + userId + "/create_user_project", projectId)
  }

  //Delete api/project/:id/user/:user_id/delete_user_project
  deleteProjectFromUser(projectId: any, userId: any) {
    return this._http.delete(this.projectsApi + "/" + projectId + "/user/" + userId + "/delete_user_project", projectId);
  }
}
