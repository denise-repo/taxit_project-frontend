import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  dashboardApi = "http://localhost:3000/api/dashboard";

  constructor(
    private _http: HttpClient
  ) {}

  //api/dashboard/users_gender
  getGenderData() {
    return this._http.get(this.dashboardApi + '/users_gender');
  }

  //api/dashboard/entry_date
  getEntryDate() {
    return this._http.get(this.dashboardApi + '/entry_date')
  }

  //api/dashboard/totals
  getTotalsData() {
    return this._http.get(this.dashboardApi + '/totals')
  }
}
