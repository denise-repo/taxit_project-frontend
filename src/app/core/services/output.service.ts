import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Output} from "../../shared/interfaces/output.interface";

@Injectable({
  providedIn: 'root'
})
export class OutputService {

  outputApi = "http://localhost:3000/api/outputs";

  constructor(
    private _http: HttpClient
  ) {
  }

  // /api/outputs/generate_output
  getOutput() {
    return this._http.get(this.outputApi + '/generate_output');
  }

  // //api/outputs/:id/download_output
  downloadOutput(outputId: number) {
    return this._http.get(this.outputApi + '/' + outputId + '/download_output')
  }
}
