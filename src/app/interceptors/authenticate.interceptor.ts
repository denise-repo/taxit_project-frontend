import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {User} from "../shared/interfaces/user.interface";

let users: User[] = [{id: 1, first_name: 'Denise', last_name: 'Machado', phone: 916453173, rank: 'rank', entry_date: '12-04-2020', email: 'de@hotmail.com',
  age: '32', gender: 'F', username: 'demachad', password: '123456', token: '1234khjgdi44567', permissions: [] }
]

@Injectable()
export class AuthenticateInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const { url, method, body } = req; // Deconstruct the request

    if(url.endsWith('/login') && method == 'POST' ) {
      const { username, password } = body;

      const user = users.find(x => x.username === username);
      if (user != undefined && password === user.password) {
        // We created a HttpResponse with a status of 200 and the body payload set to our usersData.
        // We passed the instance to the Observable#of(...) call and returns the Observable.
        return of(new HttpResponse({ status: 200, body: user }));
      } else {
        return of(new HttpResponse({ status: 401, body: null}));
      }
    } else if (url.match(/\/users\/\d+$/) && method === 'GET'){
      const urlParts = url.split('/');
      const id = parseInt(urlParts[urlParts.length - 1]);
      const user = users.find(x => x.id === id);
      return of(new HttpResponse({ status: 200, body: user }));
    } else {
      //we have all the interceptors provided, but we only use one at a time. This is done by checking the path.
      // If it is not the request we are looking for, we can pass it on to the next interceptor with next.handle(req).
      return next.handle(req);
    }
  }
}
