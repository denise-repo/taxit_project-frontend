import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from "./pages/users/users.component";
import { HomepageComponent } from "./pages/homepage/homepage.component";
import { ProjectComponent } from "./pages/project/project.component";
import { TechnologyComponent } from "./pages/technology/technology.component";
import { UserInfoComponent } from "./pages/user-info/user-info.component";
import { EditUserComponent } from "./pages/users/edit-user/edit-user.component";
import { EditProjectComponent } from "./pages/project/edit-project/edit-project.component";
import { EditTechologyComponent } from "./pages/technology/edit-techology/edit-techology.component";
import { LoginComponent } from "./pages/login/login.component";
import {AppComponent} from "./app.component";
import {ResolverService} from "./guards/resolver.service";
import {HomeGuardService} from "./guards/home-guard.service";



const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
    path: '',
    component: AppComponent,
    resolve: {user: ResolverService},
    canActivate: [HomeGuardService],
  },
      { path: 'home', component: HomepageComponent, canActivate: [HomeGuardService] },
      { path: 'login', component: LoginComponent },
      { path: 'users', component: UsersComponent },
      { path: 'edit-user/:id', component: EditUserComponent },
      { path: 'user-info/:id', component: UserInfoComponent },
      { path: 'project', component: ProjectComponent },
      { path: 'edit-project/:id', component: EditProjectComponent },
      { path: 'technology', component: TechnologyComponent },
      { path: 'edit-technology/:id', component: EditTechologyComponent },
      { path: 'user-projects', component: UserInfoComponent },
      //{ path: '**', component: PageNotFoundComponent } to implement
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
