import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import {ActivatedRoute, Router} from "@angular/router";
import {TechnologyService} from "../../../core/services/technology.service";
import {Technology} from "../../../shared/interfaces/technology.interface";

@Component({
  selector: 'app-edit-techology',
  templateUrl: './edit-techology.component.html',
  styleUrls: ['./edit-techology.component.scss']
})
export class EditTechologyComponent implements OnInit {

  id!: number;
  updateForm!: FormGroup;
  technology!: Technology;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _technolgyService: TechnologyService
  ) { }

  ngOnInit(): void {
    this.updateTechnology();
  }
  updateTechnology() {
    this.id = this._route.snapshot.params['id'];

    if (!this.id) {
      this._router.navigate(['technology']);
      return;
    }
    this.updateForm = this._formBuilder.group({
      id: [''],
      name: ['', Validators.required]
    });
    this._technolgyService.getTechnology(this.id).subscribe( data=> {
      this.updateForm.setValue(data);
      });
    }

  onSubmit() {
    this._technolgyService.updateTechnology(this.updateForm.value).pipe(first())
      .subscribe( data=> {
        this._router.navigate(['technology']);
      });
  }

  cancel(): void {
    this._router.navigate(['technology']);
  }
}
