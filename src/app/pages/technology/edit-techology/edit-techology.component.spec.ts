import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTechologyComponent } from './edit-techology.component';

describe('EditTechologyComponent', () => {
  let component: EditTechologyComponent;
  let fixture: ComponentFixture<EditTechologyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditTechologyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTechologyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
