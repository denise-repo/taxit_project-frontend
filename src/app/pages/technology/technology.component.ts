import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import { Router } from "@angular/router";
import {TechnologyService} from "../../core/services/technology.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {CreateTechnologyComponent} from "./create-technology/create-technology.component";
import {DialogService} from "../../core/services/dialog.service";
import {Technology} from "../../shared/interfaces/technology.interface";
import {User} from "../../shared/interfaces/user.interface";

@Component({
  selector: 'app-technology',
  templateUrl: './technology.component.html',
  styleUrls: ['./technology.component.scss']
})
export class TechnologyComponent implements OnInit {

  columnsToDisplay = ['name', 'created_at', 'details', 'edit', 'delete'];

  technologies: any = [];
  isClicked: boolean = false;
  technology!: Technology;

  dataSource = new MatTableDataSource<any>([]);

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private _router: Router,
    private _technologyService: TechnologyService,
    private _dialogService: DialogService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getTechnologies();
    this.dataSource = new MatTableDataSource(this.technologies);
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
    })
  }

  createTechnology() {
    this.isClicked = true;
    const dialogRef = this.dialog.open(CreateTechnologyComponent,  { width: '75%' });

    dialogRef.afterClosed().subscribe(async (result) => {
      if(!!result) {
        await this._technologyService.createTechnology({
         name: result.name,
        });
        this.getTechnologies();
      }
    });
  }

  getTechnologies() {
    this._technologyService.getTechnologiesList().subscribe( data => {
      this.technologies = data;
    });
  }

  updateTechnologyName(technology: Technology) {
    this._router.navigate([`/edit-technology/${technology.id}`])
  }

  deleteTechnology(technology: Technology): void {
    this._technologyService.deleteTechnology(technology.id).subscribe( () => {
      this.getTechnologies();
      //this._toastr.success(data)
    });
  }

  backToHomePage() {
    this._router.navigate(['/home']);
  }
}
