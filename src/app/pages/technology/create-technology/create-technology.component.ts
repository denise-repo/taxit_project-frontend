import {Component, OnInit} from '@angular/core';
import {Technology} from "../../../shared/interfaces/technology.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {UsersService} from "../../../core/services/users.service";

@Component({
  selector: 'app-create-technology',
  templateUrl: './create-technology.component.html',
  styleUrls: ['./create-technology.component.scss']
})
export class CreateTechnologyComponent implements OnInit {

  createTechnologyForm = new FormGroup({
    name: new FormControl('', Validators.required),
  });

  constructor(
    private _userService: UsersService,
    private _dialogRef: MatDialogRef<CreateTechnologyComponent>
  ) {
  }

  ngOnInit(): void {

  }

  createTechnology(): void {
    this.closeDialog(this.createTechnologyForm.value);
  }

  private closeDialog(input: any): void {
    this._dialogRef.close(input);
  }
}
