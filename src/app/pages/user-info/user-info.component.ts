import {Component, OnInit} from '@angular/core';
import {UsersService} from "../../core/services/users.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ProjectsService} from "../../core/services/projects.service";
import {User} from "../../shared/interfaces/user.interface";
import {Observable} from "rxjs";
import {DialogService} from "../../core/services/dialog.service";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  //ActivateRoutes
  user$!: Observable<any>;
  userId = this._route.snapshot.paramMap.get('id');

  columns = ['name', 'start_date', 'end_date', 'status', 'created_at', 'delete'];

  projectList: any = [];
  user!: User;
  userProjects: any = [];
  selectedProject!: any;
  listOfProjectsFromUser: any = [];
  isLoadingResults: boolean = false;

  file!: File; // Variable to store file
  importedFiles: any = [];


  constructor(
    private _userService: UsersService,
    private _router: Router,
    private _projectService: ProjectsService,
    private _route: ActivatedRoute,
    private _dialogService: DialogService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.getListOfAllProjects();
    this.showUserProjects(this.userId);
  }

  getListOfAllProjects() {
    this._projectService.getProjectList().subscribe(data => {
      this.projectList = data;
    });
  }

  onProjectSelected(selectedProjectId: any): void {
    this._projectService.userProjects(selectedProjectId, this.userId).subscribe(data => {
      this.userProjects = data;
    });
  }

  showUserProjects(userId: any) {
    this._userService.getAllProjectsFromUser(userId).subscribe(data => {
      this.userProjects = data;
    });
  }

  deleteProjectFromUser(project: any) {
    this._dialogService.openConfirmDialog('Tem a certeza que quer apagar este projecto?')
      .afterClosed().subscribe(userInput => {
      if (userInput) {
        this._projectService.deleteProjectFromUser(project.id, Number(this.userId)).subscribe(() => {
          console.log("Deleted")
        });
      }
      this.refreshTable();
    });
  }

  // On file Select
  onChange(event: any) {
    this.file = event.target.files[0];
  }

  // OnClick of button Upload
  onUpload() {
    this._userService.importUsersPdf(Number(this.userId), this.file).subscribe(() => {
      this.file;
      console.log(this.file)
    });
  }

  downloadPdf() {
    this._userService.downloadPdfAttachment(Number(this.userId)).subscribe((data: any) => {
      window.open(data['url'])
      console.log(data);
    });
  }

  async refreshTable() {
    this.isLoadingResults = true;
    this.showUserProjects(this.userId);
    this.isLoadingResults = false;
  }

  backToUsersPage() {
    this._router.navigate(['/users']);
  }

  goToProjects() {
    this._router.navigate(['/project'])
  }

}
