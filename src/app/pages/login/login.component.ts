import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticateService} from "../../core/services/authenticate.service";
import {User} from "../../shared/interfaces/user.interface";
import {NotificationService} from "../../core/services/notification.service";

@Component({
  selector: 'login-root',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  username!: string;
  password!: string;

  constructor(
    private _route: Router,
    private _authService: AuthenticateService,
    private _notificationService: NotificationService
  ) {
  }

  login() {
    this._authService.authenticate(this.username, this.password).subscribe(user => {
        if (user != null) {
          console.log(this.username, this.password)
          localStorage.setItem('token', user.token); // Save user token
          localStorage.setItem('userId', ""+user.id);
          this._route.navigate(['home']);
        } else {
          this._notificationService.showError("Username ou Password errados.");
        }
      }
    )
  }
}
