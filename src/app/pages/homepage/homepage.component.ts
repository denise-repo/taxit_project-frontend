import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {OutputService} from "../../core/services/output.service";
import {NotificationService} from "../../core/services/notification.service";
import {DashboardService} from "../../core/services/dashboard.service";
import {dashboardData, entriesChartOptions, totalChartOptions} from "../../config/charts_config";
import {ChartType} from "angular-google-charts";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  outputFile!: any;
  isLoadingResults: boolean = false;

  title: string = 'Gráficos';
  genderTitle: string = 'Percentagem de Utilizadores por Género';
  totalsTitle: string = 'Totais';
  entiesByMonthTitle: string = 'Total de entradas por mês';
  chartsTypes = ChartType;

  genderData!: any;
  totalsData!: any;
  totalEntries!: any;

  dashboardData = dashboardData;
  //object for totals chart
  totalsChart = {
    columnNames: [],
    data: [],
    options: totalChartOptions,
  };

  //object for total entries chart
  totalEntriesChart = {
    columnNames: [],
    data: [],
    options: entriesChartOptions,
  };

  constructor(
    private _router: Router,
    private _outputService: OutputService,
    private notificationService : NotificationService,
    private _dashboardService: DashboardService,
  ) { }

  ngOnInit() {
    this.getDashboardData();
  }

  userInfo() {
    this._router.navigate(['/users']);
  }

  projectInfo() {
    this._router.navigate(['/project']);
  }

  technologyInfo() {
    this._router.navigate(['/technology']);
  }

  backToLogin() {
    this._router.navigate(['/login']);
  }

  generateOutput() {
    this._outputService.getOutput().subscribe(data => {
     this.outputFile = data;
      this.notificationService.showSuccess("Output gerado com sucesso!!")
    });
  }

  downloadOutput() {
    this._outputService.downloadOutput(this.outputFile.id).subscribe((data: any) => {
      window.open(data['url'])
      console.log(data)
    });
  }

  getDashboardData() {
    this._dashboardService.getGenderData().subscribe(data =>{
      this.isLoadingResults = true;
      this.genderData = data;
    });
    this._dashboardService.getEntryDate().subscribe(data => {
      this.isLoadingResults = true;
      this.totalEntries = data;
      console.log(data);
    });
    this._dashboardService.getTotalsData().subscribe(data=> {
      this.isLoadingResults = true;
      this.totalsData = data;
    })
  }

}
