import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.scss']
})
export class CreateProjectComponent implements OnInit {



  createProjectFormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    start_date: new FormControl('', Validators.required),
    end_date: new FormControl('', Validators.required),
    status: new FormControl('', Validators.required)
  });

  constructor(
    private _dialogRef: MatDialogRef<CreateProjectComponent>
  ) { }

  ngOnInit(){
  }

  /**
   * Creates projects with params
   */
  createProject(): void {
    this.closeDialog(this.createProjectFormGroup.value);
  }

  /**
   * Close component dialog
   * @params: input
   */
  private closeDialog(input: any): void {
    this._dialogRef.close(input);

  }
}
