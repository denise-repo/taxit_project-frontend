import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ProjectsService } from "../../core/services/projects.service";
import { MatDialog } from "@angular/material/dialog";
import { CreateProjectComponent } from "./create-project/create-project.component";
import { Project } from "../../shared/interfaces/project.interface";

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  columnsToDisplay = ['name', 'start_date', 'end_date', 'status', 'details', 'edit', 'delete'];
  projects: any = [];

  isClicked: boolean = false;

  constructor(
    private _router: Router,
    private _projectService: ProjectsService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getList();
  }

  async createProject() {
    this.isClicked = true;
    const dialogRef = this.dialog.open(CreateProjectComponent,  { width: '75%' });

    dialogRef.afterClosed().subscribe(async (result) => {
      if(!!result) {
        const response = await this._projectService.createProject({
          name: result.name,
          start_date: result.start_date,
          end_date: result.end_date,
          status: result.status,
        });
        this.getList();
      }
    });
  }

  getList() {
    this._projectService.getProjectList().subscribe(data => {
      this.projects = data;
    })
  }

  //Redirects to edit project info
  updateProjectInfo(project: Project) {
    this._router.navigate([`/edit-project/${project.id}`])
  }

  deleteProject(project: Project) {
    this._projectService.deleteProject(project.id).subscribe( () => {
      this.getList();
    })
  }

  backToHomePage() {
    this._router.navigate(['/home']);
  }

}
