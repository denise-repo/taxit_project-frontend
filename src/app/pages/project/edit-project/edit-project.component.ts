import {Component, OnInit} from '@angular/core';
import {Project} from "../../../shared/interfaces/project.interface";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProjectsService} from "../../../core/services/projects.service";
import {ActivatedRoute, Router} from "@angular/router";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.scss']
})
export class EditProjectComponent implements OnInit {

  project!: Project;
  editForm!: FormGroup;
  id!: number;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _projectService: ProjectsService
  ) {
  }

  ngOnInit(): void {
    this.updateProject();
  }

  updateProject() {
    this.id = this._route.snapshot.params['id'];

    if (!this.id) {
      this._router.navigate(['project']);
      return;
    }
    this.editForm = this._formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      start_date: ['', Validators.required],
      end_date: ['', Validators.required],
      status: [{ value:'', disabled: true }, Validators.required]
    });
    this._projectService.getProject(this.id).subscribe(data => {
      this.editForm.setValue(data);
    });
  }

  onSubmit() {
    this._projectService.updateProject(this.editForm.value).pipe(first()).subscribe(data => {
      console.log(data)
      this._router.navigate(['project']);
    });
  }

  cancel(): void {
    this._router.navigate(['project'])
  }
}
