import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UsersService} from "../../core/services/users.service";
import {ActivatedRoute, Router} from "@angular/router";
import {CreateUserComponent} from "./create-user/create-user.component";
import {MatDialog} from "@angular/material/dialog";
import {User} from "../../shared/interfaces/user.interface";
import {ToastrService} from "ngx-toastr";
import {Observable} from "rxjs";
import {switchMap} from "rxjs/operators";


@Component({
  selector: 'app-user',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  columnsToDisplay = ['first_name', 'last_name', 'phone', 'email', 'rank', 'age', 'gender', 'entry_date', 'details', 'edit', 'delete'];
  users: any = [];

  isClicked: boolean = false;
  user: any;

  //Because we want to access a part of a route
  //https://angular.io/guide/router#accessing-query-parameters-and-fragments
  users$!: Observable<any>;
  selectedUserId!: number;
  isLoadingResults: boolean = false;

  file!: File; // Variable to store file


  constructor(
    private _userService: UsersService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _toastr: ToastrService,
    public dialog: MatDialog
  ) {
  }


  ngOnInit() {
    this.users$ = this._route.paramMap.pipe(
      switchMap(params => {
        this.selectedUserId = Number(params.get('id'));
        return this._userService.getUsersList();
      })
    );
    this.getAllUsers();
  }

  async createNewUser() {
    this.isClicked = true;
    const dialogRef = this.dialog.open(CreateUserComponent, {width: '75%'});

    dialogRef.afterClosed().subscribe(async (result) => {
      if (!!result) {
        const response = await this._userService.createUser({
          first_name: result.first_name,
          last_name: result.last_name,
          phone: result.phone,
          email: result.email,
          rank: result.rank,
          entry_date: result.entry_date,
          age: result.age,
          gender: result.gender
        });
        this.getAllUsers();
      }
    });
  }

  getAllUsers() {
    this._userService.getUsersList().subscribe(data => {
      this.users = data;
    })
  }

  // On file Select
  onChange(event: any) {
    this.file = event.target.files[0];
  }

  // OnClick of button Upload
  onUpload() {
    this._userService.importFile(this.file).subscribe((event: any) => {
      if (typeof (event) === 'object') {
        this.refreshTable();
      }
    });
  }

  async refreshTable() {
    this.isLoadingResults = true;
    this.getAllUsers();
    this.isLoadingResults = false;
  }

  backToHomePage() {
    this._router.navigate(['/home']);
  }

  //Redirects to user info component
  seeUserInfo(user: User) {
    this._router.navigate([`/user-info/${user.id}`]);
    console.log("User_route ===>", `/user-info/${user.id}`)
  }

  //Redirects to edit user info
  updateUserInfo(user: User) {
    this._router.navigate([`/edit-user/${user.id}`])
  }

  deleteUser(user: User): void {
    this._userService.deleteUser(user.id).subscribe(() => {
      this.getAllUsers();
      //this._toastr.success(data)
    });
  }


}
