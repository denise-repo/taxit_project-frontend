import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { UsersService } from "../../../core/services/users.service";
import { MatDialogRef } from "@angular/material/dialog";

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  createUserFormGroup = new FormGroup({
    first_name: new FormControl('', Validators.required),
    last_name: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    rank: new FormControl('', Validators.required),
    entry_date: new FormControl('', Validators.required),
    gender: new FormControl('', Validators.required),
    age: new FormControl('', Validators.required)
  });

  constructor(
    private _userService: UsersService,
    private _dialogRef: MatDialogRef<CreateUserComponent>
  ) {
  }

  ngOnInit(): void {

  }

  /**
   * Creates users with params
   */
  createUser(): void {
    this.closeDialog(this.createUserFormGroup.value);
  }

  /**
   * Close component dialog
   * @params: input
   */
  private closeDialog(input: any): void {
    this._dialogRef.close(input);
  }
}
