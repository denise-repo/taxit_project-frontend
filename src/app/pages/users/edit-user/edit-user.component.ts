import { Component, OnInit } from '@angular/core';
import { User } from "../../../shared/interfaces/user.interface";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { UsersService } from "../../../core/services/users.service";
import { first } from "rxjs/operators";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  user!: User;
  editForm!: FormGroup;
  id!: any;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _userService: UsersService
  ) {}

  ngOnInit(): void {
    this.userToUpdate();
  }

  userToUpdate() {
    this.id = this._route.snapshot.params['id'];

    if (!this.id) {
      this._router.navigate(['users']);
      return;
    }
    this.editForm = this._formBuilder.group({
      id: [''],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      phone: ['', [Validators.required, Validators.pattern('^\\d{9}')]],
      email: ['', [Validators.required, Validators.email]],
      rank: ['', Validators.required],
      entry_date: ['', Validators.required],
      age: [''],
      gender: ['']
    });
    this._userService.getUser(this.id).subscribe(data => {
      this.editForm.setValue(data);
    });
  }

  onSubmit() {
    this._userService.updateUser(this.editForm.value).pipe(first()).subscribe(data => {
        this._router.navigate(['users']);
    });
  }

  cancel(): void {
    this._router.navigate(['users'])
  }
}
